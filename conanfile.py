from conans import ConanFile, CMake


class CascrelConan(ConanFile):
    name = "cascrel"
    version = "0.1.0-5"
    license = "MIT"
    author = "Marcin Puc marcin.e.puc@gmail.com"
    homepage = "https://gitlab.com/crel/cascrel"
    url = "https://gitlab.com/crel/conan-cascrel.git"
    description = "Implementation of the Cascade Correlation architecture"
    topics = ("scientific software", "neural network")
    settings = "os", "compiler", "build_type", "arch"
    options = {}
    default_options = {}
    generators = "cmake"
    requires = "eigen/[~=3.3]@conan/stable", "spdlog/[~=1.4]@bincrafters/stable"
    build_requires = ("Catch2/[~=2]@catchorg/stable",)

    def __configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["CASCREL_USE_CONAN"] = True
        cmake.configure(source_folder="cascrel")
        return cmake

    def source(self):
        self.run("git clone -b {} https://gitlab.com/crel/cascrel.git".format(self.version))
        # This small hack might be useful to guarantee proper /MT /MD linkage
        # in MSVC if the packaged project doesn't have variables to set it
        # properly

    def build(self):
        cmake = self.__configure_cmake()
        cmake.build(target="cascrel_test")
        cmake.test()

        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        cmake = self.__configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["libcascrel.so"]
        self.cpp_info.includedirs = ["include/cascrel"]
