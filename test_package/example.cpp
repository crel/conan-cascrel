#include <iostream>

#include "CascadeCorrelationNetwork.hpp"
#include "factory/Builder.hpp"

using cascrel::factory::Builder;
using cascrel::CascadeCorrelationNetwork;

int main() {
    Builder builder;
    CascadeCorrelationNetwork ccn = builder.buildWithDimensions(10, 20);

    return 0;
}
